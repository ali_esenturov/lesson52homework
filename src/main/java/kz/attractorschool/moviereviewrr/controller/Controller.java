package kz.attractorschool.moviereviewrr.controller;

import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.model.Review;
import kz.attractorschool.moviereviewrr.model.User;
import kz.attractorschool.moviereviewrr.repository.MovieRepository;
import kz.attractorschool.moviereviewrr.repository.ReviewRepository;
import kz.attractorschool.moviereviewrr.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    MovieRepository movieRepository;

    //users
    
    @GetMapping("/user/{email}")
    public boolean existsByEmail(@PathVariable("email") String email){
        return userRepository.existsByEmail(email);
    }

    @GetMapping("/user/{name}")
    public Iterable<User> getUserByName(@PathVariable("name") String name) {
        return userRepository.findByName(name);
    }
    
    //reviews
    
    @GetMapping("/review/{movie_id}")
    public Iterable<Review> getReviewByMovieId(@PathVariable("movie_id") String id) {
        return reviewRepository.findAllByMovie_Id(id);
    }

    @GetMapping("/review/{movie_title}")
    public Iterable<Review> getReviewByMovieTitle(@PathVariable("movie_title") String title) {
        return reviewRepository.findAllByMovie_Title(title);
    }

    @GetMapping("/review/{reviewer_id}")
    public Iterable<Review> getReviewByUserId(@PathVariable("reviewer_id") String id) {
        return reviewRepository.findAllByReviewer_Id(id);
    }

    @GetMapping("/review/{reviewer_name}")
    public Iterable<Review> getReviewByUserName(@PathVariable("reviewer_name") String name) {
        return reviewRepository.findAllByReviewer_Name(name);
    }
    
    
    //movies

    @GetMapping("/movie")
    public Iterable<Movie> getMovie() {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return movieRepository.findAll(s);
    }

    @GetMapping("/movienew/{year}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return movieRepository.findAllByReleaseYearGreaterThanEqual(year, s);
    }

    @GetMapping("/movienew/{year}/{year2}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year,
                                    @PathVariable("year2") int year2) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return movieRepository.findAllByReleaseYearBetween(year, year2, s);
    }

    @GetMapping("/moviebetween/{year}/{year2}")
    public Iterable<Movie> getMovieBetween(@PathVariable("year") int year,
                                           @PathVariable("year2") int year2) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return movieRepository.getMoviesBetween(year, year2, s);
    }

    @GetMapping("/movienew/{actor}")
    public Iterable<Movie> getMovieByActor(@PathVariable("actor") String actor) {
        return movieRepository.findAllByActors(actor);
    }

    @GetMapping("/movienew/{title}")
    public Iterable<Movie> getMovieByTitle(@PathVariable("title") String title) {
        return movieRepository.findAllByTitle(title);
    }

    @GetMapping("/movienew/{director}")
    public Iterable<Movie> getMovieByDirector(@PathVariable("director") String director) {
        return movieRepository.findAllByDirectors(director);
    }
}
